import * as express from 'express';
import { createServer, Server } from 'http';
import * as Bundler from 'parcel-bundler';
import { join } from 'path';
import * as createIO from 'socket.io';

import { proxyBoard } from '../src/server/proxy-board';

const app = express();
const server = createServer(app);

const PORT = 1234;

proxyBoard(server);

// Set up Parcel bundler
const clientEntryPath = join(process.cwd(), 'example', 'index.html');
const clientBundler = new Bundler(clientEntryPath);
app.use(clientBundler.middleware());

// Attempt to gracefully shut down
process.on('SIGTERM', () => server.close(() => process.exit(0)));

server.listen(PORT, () => console.log(`http://localhost:${PORT}/`));
