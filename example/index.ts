import { connectProxy } from '../src/client';
import { button, spectator } from '../src/client/io';

connectProxy('abc');

console.log('hello world!');

const playerOne = button({ name: 'player one', pin: 9, pullup: true });
const playerTwo = spectator(playerOne);

playerOne.connect();

playerOne.press.subscribe(() => console.log('button pressed'));
playerTwo.press.subscribe(() => console.log('spectator pressed'));
