# Gadzooks

`This package is a work in progress!`

A modern re-write of [Noduino](https://sbstjn.com/noduino/) in TypeScript, with an API inspired by [Johnny-Five](http://johnny-five.io/).

The purpose of this package is to make multiplayer JavaScript experiences incorporating Arduino sensors. Further documentation coming soon.
