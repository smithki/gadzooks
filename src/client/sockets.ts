// Node modules
import * as createIOClient from 'socket.io-client';

// Local modules
import { BOARD_PORT, PROXY_PORT } from '~shared/constants';

// Connect to WebSockets
const socket = createIOClient(`http://localhost:${BOARD_PORT}`, {
  path: '/gadzooks-board',
  reconnection: false,
});

const proxySocket = createIOClient(undefined, {
  path: '/gadzooks-proxy',
});

export { socket, proxySocket };
