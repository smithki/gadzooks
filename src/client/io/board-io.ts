// Node modules
import * as uuidV4 from 'uuid';

// Local modules
import { pinEventObservable } from '~client/io/pin-event-observable';
import { socket } from '~client/sockets';
import { PinRegistration } from '~server/types';
import { readonly } from '~shared/readonly';

export interface IoOptions {
  name: string;
  pin: string | number;
}

export interface BoardIoOptions extends IoOptions {
  type: string;
  events: string[];
  options: any;
  digital?: boolean;
}

export interface BoardIo {
  connected: boolean;
  id: string;
  readonly pin: string;
  readonly events: string[];
  /** */
  readonly connect: () => Promise<void>;
  /** */
  readonly disconnect: () => Promise<void>;
}

function sanitizePin({ digital, pin }) {
  if (typeof pin === 'string') return pin;
  return (digital ? 'D' : 'A') + pin;
}

/** */
function boardIo(options: BoardIoOptions): BoardIo {
  const {
    name,
    type,
    pin,
    events,
    options: j5Options,
    digital = false,
  } = options;

  //
  const result: Partial<BoardIo> = {
    id: name,
    // id: uuidV4(),
    connected: false,
  };

  //
  const connect = () => {
    if (result.connected) return Promise.resolve();
    return new Promise(resolve => {
      const pinRegistration: PinRegistration = {
        type,
        events,
        options: j5Options,
        id: result.id,
      };

      socket.emit('register', pinRegistration, () => {
        result.connected = true;
        resolve();
      });
    });
  };

  //
  const disconnect = () => {
    if (!result.connected) return Promise.resolve();
    return new Promise(resolve => {
      socket.emit('deregister', { type, options, events }, () => {
        result.connected = false;
        resolve();
      });
    });
  };

  //
  const eventObservables = {};
  for (const e of events) {
    eventObservables[e] = pinEventObservable(result.id, e, socket);
  }

  //
  return Object.assign(
    result,
    readonly(result, {
      connect,
      disconnect,
      events,
      pin: sanitizePin({ digital, pin }),
      ...eventObservables,
    }),
  );
}

export { boardIo };
