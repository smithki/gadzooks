// Node modules
import { Observable } from 'rxjs';

// Local modules
import { boardIo, BoardIo, IoOptions } from '~client/io/board-io';
import { originSocket } from '~client/sockets';
import { PinPayload } from './pin-event-observable';

interface SensorOptions extends IoOptions {}

export interface Sensor extends BoardIo {
  data: Observable<PinPayload>;
}

/** */
function button(options: SensorOptions): Sensor {
  const { pin } = options;
  return boardIo({
    type: 'sensor',
    events: ['press', 'release', 'hold'],
    options: {
      pin,
    },
  });
}

export { button };
