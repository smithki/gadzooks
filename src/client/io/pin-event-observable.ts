// Node modules
import { Observable } from 'rxjs';

// Local modules
import { Payload } from '~server/types';

/** Create observables for a series of pin events. */
function pinEventObservable(
  thisId: string,
  thisEvent: string,
  socket: SocketIOClient.Socket,
  condition?: () => boolean,
) {
  const result: Observable<any> = Observable.create(observer => {
    socket.on('pin event', ({ id, data, event }: Payload) => {
      const customCondition = condition ? condition() : true;
      if (thisId === id && customCondition) {
        if (thisEvent === event) observer.next(data);
      }
    });
  });

  return result;
}

export { pinEventObservable };
