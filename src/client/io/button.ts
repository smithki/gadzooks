// Node modules
import { Observable } from 'rxjs';

// Local modules
import { boardIo, BoardIo, IoOptions } from '~client/io/board-io';

interface ButtonOptions extends IoOptions {
  pullup?: boolean;
}

interface Button extends BoardIo {
  press: Observable<null>;
  release: Observable<null>;
  hold: Observable<null>;
}

/** */
function button(options: ButtonOptions): Button {
  const { name, pin, pullup = false } = options;
  return boardIo({
    name,
    pin,
    type: 'button',
    events: ['press', 'release', 'hold'],
    options: {
      pin,
      isPullup: pullup,
    },
    digital: true,
  }) as Button;
}

export { button, Button };
