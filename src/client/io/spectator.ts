// Node modules
import { omit } from 'lodash';
import { Omit } from 'type-zoo';

// Local modules
import { proxySocket } from '~client/sockets';
import { Payload } from '~server/types';
import { BoardIo } from './board-io';
import { pinEventObservable } from './pin-event-observable';

type Spectator<T extends BoardIo> = Omit<
  { [P in keyof T]: T[P] },
  'connect' | 'disconnect'
>;

/** */
function spectator<T extends BoardIo>(ioObject: T): Spectator<T> {
  const { id, events } = ioObject;
  const result = Object.assign({}, omit(ioObject, 'connect')) as Spectator<T>;
  const customCondition = () => !ioObject.connected;
  for (const e of events) {
    result[e] = pinEventObservable(id, e, proxySocket, customCondition);
    ioObject[e].subscribe(data => {
      const payload: Payload = { id, data, event: e };
      proxySocket.emit('pin event', { payload });
    });
  }
  return result;
}

export { spectator };
