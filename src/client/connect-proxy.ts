// Local modules
import { proxySocket } from '~client/sockets';

let connected = false;

/** */
function connectProxy(sessionId) {
  if (connected) return Promise.resolve();
  return new Promise(resolve => {
    proxySocket.on('connect', () => {
      proxySocket.emit('join proxy', { sessionId }, () => {
        connected = true;
        resolve();
      });
    });
  });
}

export { connectProxy };
