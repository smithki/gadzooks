import { Server, Socket } from 'socket.io';

export interface Endpoint {
  (io: Server, socket: Socket): any;
}

export interface Payload {
  id: string;
  event: string;
  data?: any;
}

export interface ApiHandler<PropTypes> {
  (props: PropTypes, ack?: (...args: any[]) => any): void;
}

export interface PinRegistration {
  type: string;
  options: any;
  id: string;
  events: string[];
}

export interface ProxyRegistration {
  id: string;
}
