// Node modules
import { Server } from 'http';
import * as createIo from 'socket.io';

// Local modules
import { PROXY_PORT } from '~shared/constants';
import { proxyApi } from './api';

const sessions = new Map();

/** */
function proxyBoard(server: Server) {
  const io = createIo(server, {
    path: '/gadzooks-proxy',
  });
  proxyApi(io);
}

export { proxyBoard };
