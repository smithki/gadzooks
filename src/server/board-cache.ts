import { Button, Sensor } from 'johnny-five';
import { cleanListeners } from '~server/clean-listeners';

type J5Object = Button | Sensor;
type BoardCache = Map<string, J5Object>;

// Cache for active J5 objects
const boardCache: BoardCache = new Map();

/** */
function clearBoardCache() {
  boardCache.forEach((value, key) => {
    cleanListeners(value as any);
    boardCache.delete(key);
  });
}

export { boardCache, clearBoardCache };
