import { Button, Sensor } from 'johnny-five';

import { boardCache } from '~server/board-cache';
import { ApiHandler, Endpoint, Payload, PinRegistration } from '~server/types';

//
const register: Endpoint = (io, socket) => {
  const handler: ApiHandler<PinRegistration> = (
    { type, id, options, events },
    ack,
  ) => {
    let j5;
    switch (type) {
      case 'sensor':
        j5 = new Sensor(options);
        break;
      case 'button':
        j5 = new Button(options);
        break;
    }

    boardCache.set(id, j5);

    for (const e of events) {
      j5.on(e, data => {
        const payload = { data, id, event: e };
        io.emit('pin event', payload);
      });
    }

    ack();
  };

  socket.on('register', handler);
};

export { register };
