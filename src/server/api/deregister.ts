import { boardCache } from '~server/board-cache';
import { cleanListeners } from '~server/clean-listeners';
import { ApiHandler, Endpoint } from '~server/types';

//
const deregister: Endpoint = (io, socket) => {
  const handler: ApiHandler<{ id: string }> = ({ id }, ack) => {
    const j5 = boardCache.get(id) as any;
    cleanListeners(j5);
    boardCache.delete(id);
    ack();
  };

  socket.on('deregister', handler);
};

export { deregister };
