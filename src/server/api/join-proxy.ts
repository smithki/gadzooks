import { ApiHandler, Endpoint, Payload } from '~server/types';

const joinProxy: Endpoint = (io, socket) => {
  const handler: ApiHandler<{ sessionId: string }> = ({ sessionId }, ack) => {
    console.log('socket joined', socket.id);
    const session = io.to(sessionId);
    socket.join(sessionId);
    socket.on('pin event', ({ payload }: { payload: Payload }) => {
      session.emit('pin event', payload);
    });
    ack();
  };

  socket.on('join proxy', handler);
};

export { joinProxy };
