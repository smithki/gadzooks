// Node modules
import { Button, Sensor } from 'johnny-five';
import { Server } from 'socket.io';

// Local modules
import { Endpoint } from '~server/types';
import { deregister } from './deregister';
import { joinProxy } from './join-proxy';
import { register } from './register';

function initApi(io: Server) {
  return (...endpoints: Endpoint[]) => {
    io.on('connection', socket => {
      for (const endpoint of endpoints) endpoint(io, socket);
    });
  };
}

/** Initialize WebSockets API */
const boardApi = (io: Server) => initApi(io)(register, deregister);
/** Initialize WebSockets proxy API */
const proxyApi = (io: Server) => initApi(io)(joinProxy);

export { boardApi, proxyApi };
