import { EventEmitter } from 'events';

/** */
function cleanListeners<T extends EventEmitter>(emitter: T) {
  const events = emitter.eventNames();
  for (const e of events) {
    emitter.removeAllListeners(e);
  }
}

export { cleanListeners };
