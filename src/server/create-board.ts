// Node modules
import { createServer } from 'http';
import { Board } from 'johnny-five';
import * as createIO from 'socket.io';

// Local modules
import { clearBoardCache } from '~server/board-cache';
import { BOARD_PORT } from '~shared/constants';
import { boardApi } from './api';

/** */
function createBoard(): Promise<void> {
  return new Promise((resolve, reject) => {
    // Connect to board and resolve when successful
    const board = new Board({ repl: false });
    board.on('ready', () => {
      // Create server
      const server = createServer();

      // Create WebSocket
      const io = createIO(server, {
        path: '/gadzooks-board',
      });
      io.on('connection', () => clearBoardCache());

      // Attempt to gracefully shut down
      process.on('SIGTERM', () => {
        io.close();
        server.close(() => process.exit(0));
      });

      // Listen on the server
      server.listen(BOARD_PORT, () => {
        boardApi(io);
        resolve();
      });
    });
  });
}

export { createBoard };
