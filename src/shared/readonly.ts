const isArray = Array.isArray;
const defProps = Object.defineProperties;
const assign = Object.assign;

/** */
function readonly(obj, properties: { [key: string]: any } | string[]) {
  const propMap = {};

  if (isArray(properties)) {
    for (const prop of properties) {
      propMap[prop] = { writable: false, enumerable: true };
    }
  } else {
    for (const prop in properties) {
      propMap[prop] = {
        value: properties[prop],
        enumerable: true,
        writable: false,
      };
    }
  }

  const result = assign({}, obj);
  return defProps(result, propMap);
}

export { readonly };
